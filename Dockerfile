FROM node:10 as builder

WORKDIR /data
ADD ./package.json /data/package.json
RUN yarn config set registry https://registry.npm.taobao.org --global
RUN npm install

ADD . /data
RUN npm run dev

FROM nginx
COPY --from=builder /data/dist /opt/vue-element-admin/
COPY nginx.conf /etc/nginx/conf.d/default.conf
